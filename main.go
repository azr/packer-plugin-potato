package main

import (
	"fmt"
	"os"

	"gitlab.com/azr/packer-plugin-potato/builder/potato"
	potatoVersion "gitlab.com/azr/packer-plugin-potato/version"

	"github.com/hashicorp/packer-plugin-sdk/plugin"
)

func main() {
	pps := plugin.NewSet()
	pps.RegisterBuilder(plugin.DEFAULT_NAME, new(potato.Builder))
	pps.SetVersion(potatoVersion.PluginVersion)
	err := pps.Run()
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}
}

source "potato" "basic-example" {
  potato = "🥔"
}

build {
  sources = [
    "source.potato-my-builder.basic-example"
  ]

  provisioner "shell-local" {
    inline = [
      "echo build generated data: ${build.GeneratedMockData}",
    ]
  }
}

package potato

import (
	"context"
	_ "embed"

	"github.com/hashicorp/packer-plugin-sdk/multistep"
	packersdk "github.com/hashicorp/packer-plugin-sdk/packer"
)

//go:embed "potato.txt"
var potato string

type StepPotato struct {
	MockConfig string
}

func (s *StepPotato) Run(_ context.Context, state multistep.StateBag) multistep.StepAction {
	ui := state.Get("ui").(packersdk.Ui)

	ui.Say(potato)

	return multistep.ActionContinue
}

func (s *StepPotato) Cleanup(_ multistep.StateBag) {
	// potatoes are clean
}

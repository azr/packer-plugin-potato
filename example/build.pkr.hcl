packer {
  required_plugins {
    potato = {
      version = ">=v0.1.0"
      source  = "github.com/hashicorp/potato"
    }
  }
}

source "potato" "bar-example" {
  mock   = "Mock"
  potato = 🥔
}

build {
  sources = [
    "source.potato-my-builder.foo-example",
  ]

}
